from random import randint
import math
from copy import copy


class Vector2d:
    def __gt__(self, other):
        return self.__len__() > other.__len__()

    def __ge__(self, other):
        return self.__len__() >= other.__len__()

    def __eq__(self, other):
        return (self.x == other.x) and (self.y == other.y)

    def __lt__(self, other):
        return self.__len__() < other.__len__()

    def __le__(self, other):
        return self.__len__() <= other.__len__()

    def __len__(self):
        return math.sqrt(self.x * self.x + self.y * self.y)

    def __add__(self, other):
        g = copy(self)
        if type(other) is self.__class__:
            g.move_on(other.x, other.y)
        elif type(other) is int:
            g.move_on(other, other)
        else:
            raise Exception("TypeError: Vector2d+Vector2d or Vector2d+int")
        return g

    def __sub__(self, other):
        g = copy(self)
        if type(other) is self.__class__:
            g.move_on(-other.x, -other.y)
        elif type(other) is int:
            g.move_on(-other, -other)
        else:
            raise Exception("TypeError: Vector2d+Vector2d or Vector2d+int")
        return g

    def __mul__(self, other):
        g = copy(self)
        if type(other) is self.__class__:
            g.move_on(self.x * other.x - self.x, self.y * other.y - self.y)
        elif type(other) is int:
            g.move_on(self.x * other - self.x, self.y * other - self.y)
        else:
            raise Exception("TypeError: Vector2d*Vector2d or Vector2d*int")
        return g

    def __truediv__(self, other):
        g = copy(self)
        if type(other) is self.__class__:
            g.move_on(self.x // other.x - self.x, self.y // other.y - self.y)
        elif type(other) is int:
            g.move_on(self.x // other - self.x, self.y // other - self.y)
        else:
            raise Exception("TypeError: Vector2d*Vector2d or Vector2d*int")
        return g

    def __str__(self):
        return "{} {}".format(self.x, self.y)

    def __init__(self, x, y, parent):
        self.par = parent
        if not (type(x) is int) or not (type(y) is int):
            raise Exception("TypeError: Vector2d(int x, int y).")
        self.x = x
        self.y = y

    def move_on(self, dx, dy):
        if not (type(dx) is int) or not (type(dy) is int):
            raise Exception("TypeError: Vector2d.moveOn(int x, int y).")
        if self.par.is_infinity:
            self.x += dx
            self.y += dy
        if self.par.is_limitation:
            self.x = max(min(self.par.right_bottom.x, self.x + dx), self.par.left_top.x)
            self.y = max(min(self.par.right_bottom.y, self.y + dy), self.par.left_top.y)
        if self.par.is_cycle:
            self.x = (self.x - self.par.left_top.x + dx) % (self.par.right_bottom.x - self.par.left_top.x + 1) + self.par.left_top.x
            self.y = (self.y - self.par.left_top.y + dy) % (self.par.right_bottom.y - self.par.left_top.y + 1) + self.par.left_top.y


class Tools:
    def __init__(self):
        self.EMPTY = 0
        self.EAT = 1
        self.SNAKE = 2

        self.left_top = None
        self.right_bottom = None
        self.is_set_area = False
        self.is_cycle = False
        self.is_limitation = False
        self.is_infinity = True

    def set_area(self, left_top_buff, right_bottom_buff):
        if (not (type(left_top_buff) is Vector2d)) or (not (type(right_bottom_buff) is Vector2d)):
            raise Exception("TypeError: The variable must be Vector2d.")
        if left_top_buff > right_bottom_buff:
            raise Exception("AssociationError: The length of the vector left_up must be less than the length of the "
                            "vector right_bottom.")
        self.left_top = left_top_buff
        self.right_bottom = right_bottom_buff - 1
        self.is_set_area = True

    def set_infinity(self):
        self.is_cycle = False
        self.is_limitation = False
        self.is_infinity = True

    def set_cycle(self):
        if self.is_set_area:
            self.is_cycle = True
            self.is_limitation = False
            self.is_infinity = False
        else:
            raise Exception("AreaError: First use 'setArea'.")

    def set_limitation(self):
        if self.is_set_area:
            self.is_cycle = False
            self.is_limitation = True
            self.is_infinity = False
        else:
            raise Exception("AreaError: First use 'setArea'.")

    def get_random_vector2d(self) -> Vector2d:
        x = randint(self.left_top.x, self.right_bottom.x)
        y = randint(self.left_top.y, self.right_bottom.y)
        return Vector2d(x, y, self)
