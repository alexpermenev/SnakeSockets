import sys
import socket
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class Example(QWidget):
    def __init__(self):
        super().__init__()

        self.sockRepaint = socket.socket()
        self.sockControl = socket.socket()
        self.is_connect = False
        self.menu = QWidget(self)
        self.text = QLineEdit(self.menu)
        self.text.setGeometry(20, 20, 150, 20)
        self.text.setText("Password server")
        self.but = QPushButton(self.menu)
        self.but.setGeometry(190, 20, 80, 20)
        self.but.setText("Connect")
        self.but.clicked.connect(self.connectServ)

        self.qp = QPainter()
        self.initUI()

    def connectServ(self):
        try:
            HOST, PORT = self.text.text().split(':')
            PORT = int(PORT)
            self.sockRepaint.connect((HOST, PORT))
            self.sockControl.connect((HOST, PORT + 1))
            self.menu.setHidden(True)
            self.is_connect = True
            t = QTimer(self)
            t.setInterval(1)
            t.timeout.connect(self.repaint)
            t.start()
            self.setFocusProxy(self)
        except Exception as ex:
            self.text.setText("Error")

    def initUI(self):
        self.setGeometry(0, 0, 1920, 1080)
        self.setStyleSheet("background-color: white")
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.show()

    def paintEvent(self, e: QPaintEvent):
        self.qp.begin(self)
        if self.is_connect:
            self.sockRepaint.send("get_snake".encode())
            data = self.sockRepaint.recv(1024).decode()
            if data != 'no':
                mas = [list(map(int, i.split())) for i in data.split('|')]
                self.qp.setBrush(QColor(0, 0, 255))
                for i in mas[:-1]:
                    self.qp.drawRect(i[0] * 60, i[1] * 60, 60, 60)
                self.qp.setBrush(QColor(0, 255, 0))
                self.qp.drawRect(mas[-1][0] * 60, mas[-1][1] * 60, 60, 60)
            else:
                font = QFont()
                font.setFamily("Courier")
                font.setBold(True)
                font.setWeight(72)
                self.qp.setFont(font)
                self.qp.drawText(0, 0, 1920, 1080, Qt.AlignCenter, "Please wait start the game")
        self.qp.end()

    def keyPressEvent(self, e: QKeyEvent):
        if e.key() == Qt.Key_Escape:
            self.sockRepaint.send("close".encode())
            self.sockControl.send("close".encode())
            app.exit()
        if self.is_connect:
            if e.key() == Qt.Key_W:
                self.sockControl.send("0".encode())
                self.sockControl.recv(1024)
            elif e.key() == Qt.Key_D:
                self.sockControl.send("1".encode())
                self.sockControl.recv(1024)
            elif e.key() == Qt.Key_S:
                self.sockControl.send("2".encode())
                self.sockControl.recv(1024)
            elif e.key() == Qt.Key_A:
                self.sockControl.send("3".encode())
                self.sockControl.recv(1024)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
